<?php

require_once __DIR__ . '/../src/autoload.php';

use \MaximLoboda\Utils\FileGenerator;

$generator = new FileGenerator();
$generator->generate();