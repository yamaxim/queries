<?php

namespace MaximLoboda\Utils;

use PDOException;
use PDO;

/**
 * Class SqlGenerator - helps me to generate test data in mysql
 * @package Text
 */
class SqlGenerator
{
    /**
     * @var PDO connection object
     */
    protected $pdo;

    /**
     * @var string DB host
     */
    private $host = '172.18.0.3';

    /**
     * @var string Database name
     */
    private $db = 'yandex';

    /**
     * @var string User name
     */
    private $user = 'root';

    /**
     * @var string User password
     */
    private $password = 'secret';

    /**
     * SqlGenerator constructor.
     */
    public function __construct()
    {
        $this->checkCli();
        $this->connect();
    }

    /**
     * @return bool
     */
    public function generateUsers()
    {
        $users = [];

        $this->pdo->beginTransaction();

        for($i = 0; $i < 100000; $i++){
            $name = self::randomWord(rand(3, 10));
            $email = self::randomWord(rand(3, 10)) . '@' . self::randomWord(rand(3, 10)) . '.' . self::randomWord(rand(2, 3));
            $users[] = '(\'' . $name . '\', \'' . $email . '\')';
        }

        $queryString = 'INSERT INTO `yandex`.`clients` (`name`, `email`) VALUES ' . implode(',', $users);
        $pdoQuery = $this->pdo->prepare($queryString);
        $pdoQuery->execute();

        return (bool) $this->pdo->commit();
    }

    /**
     * @return bool
     */
    public function generateProducts()
    {
        $products = [];

        $this->pdo->beginTransaction();

        for($i = 0; $i < 100000; $i++){
            $name = self::randomWord(rand(3, 10));
            $cost = rand(2, 6);
            $description = self::randomWord(50, 60);
            $products[] = '(\'' . $name . '\', \'' . $cost . '\', \'' . $description . '\')';
        }

        $queryString = 'INSERT INTO `yandex`.`merchandise` (`name`, `cost`, `description`) VALUES ' . implode(',', $products);
        $pdoQuery = $this->pdo->prepare($queryString);
        $pdoQuery->execute();

        return (bool) $this->pdo->commit();
    }

    /**
     * @return bool
     */
    public function generateOrders()
    {
        $maxUser = 0;
        $maxProduct = 0;
        $orders = [];
        $statuses = ['preparing', 'dispatched', 'complete', 'cancelled'];

        // get users
        $sql = 'SELECT MAX(id) FROM `yandex`.`merchandise` ORDER BY `id`';
        foreach ($this->pdo->query($sql, PDO::FETCH_NUM) as $row) {
            $maxUser = (int) $row[0];
        }

        // get products
        $sql = 'SELECT MAX(id) FROM `yandex`.`merchandise` ORDER BY `id` LIMIT 10';
        foreach ($this->pdo->query($sql, PDO::FETCH_NUM) as $row) {
            $maxProduct = (int) $row[0];
        }

        $this->pdo->beginTransaction();

        for($i = 0; $i < 100000; $i++){
            $clientId = rand(1, $maxUser);
            $itemId = rand(1, $maxProduct);
            $comment = self::randomWord(50, 60);
            $unixTimeStampValue = rand(1262055681, time());
            $orderDate = date("Y-m-d H:i:s", $unixTimeStampValue);
            $status = $statuses[rand(0, 3)];
            $orders[] = '(\''
                . $clientId . '\', \''
                . $itemId . '\', \''
                . $comment . '\', \''
                . $orderDate . '\', \''
                . $status . '\')';
        }

        $queryString = 'INSERT INTO `yandex`.`orders` (`client_id`, `item_id`, `comment`, `order_date`, `status`) VALUES ' . implode(',', $orders);
        $pdoQuery = $this->pdo->prepare($queryString);
        $pdoQuery->execute();

        return (bool) $this->pdo->commit();
    }

    /**
     *
     */
    protected function connect()
    {
        try {
            $pdo = new PDO(
                'mysql:host=' . $this->host . ';dbname=' . $this->db,
                $this->user,
                $this->password
            );
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
            die();
        }
        $this->pdo = $pdo;
    }

    /**
     *
     */
    protected function checkCli()
    {
        if (php_sapi_name() !== "cli") {
            echo "Hey! Use cli instead.";
            die();
        }
    }
}