<?php

namespace MaximLoboda\Utils;

/**
 * Class FileGenerator - csv generator
 * @package Text
 */
class FileGenerator
{
    /**
     * @var string path to file
     */
    protected $path = '../cli/files/';

    /**
     * @var string file name
     */
    protected $name = 'sale.csv';

    /**
     * generate method
     */
    public function generate()
    {
        $filePath = $this->path . $this->name;

        $fp = fopen($filePath, 'w');

        for($i = 0; $i < 222; $i++){
            $clientId = mt_rand(1, 10000000);
            $orderId = mt_rand(1, 10000000);
            $comment = Text::randomWord(rand(50, 100));
            $line = [$clientId, $orderId, $comment];
            fputcsv($fp, $line);
        }

        fclose($fp);
    }
}