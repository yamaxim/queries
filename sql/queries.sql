# 1. Выбрать имена (name) всех клиентов, которые не делали заказы в последние 7 дней.
SELECT DISTINCT clients.name
FROM clients
   INNER JOIN orders ON orders.client_id = clients.id
WHERE
   DATE(orders.order_date) < DATE(NOW()) - INTERVAL 7 DAY

# 2. Выбрать имена (name) 5 клиентов, которые сделали больше всего заказов в магазине.
SELECT name FROM orders
LEFT JOIN clients ON orders.client_id = clients.id
GROUP BY orders.client_id
ORDER BY COUNT(orders.client_id) DESC
LIMIT 5

# 3. Выбрать имена (name) 10 клиентов, которые сделали заказы на наибольшую сумму.
SELECT clients.name FROM orders
INNER JOIN merchandise
	ON merchandise.id = orders.item_id
LEFT JOIN clients
	ON clients.id = orders.client_id
GROUP BY orders.client_id
ORDER BY MAX(merchandise.cost) DESC
LIMIT 10

# 4. Выбрать имена (name) всех товаров, по которым не было доставленных заказов (со статусом complete).
SELECT DISTINCT merchandise.name
FROM merchandise
INNER JOIN
	orders ON orders.item_id = merchandise.id
    WHERE NOT orders.status = 'complete'