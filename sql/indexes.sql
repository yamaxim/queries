CREATE INDEX `idx_orders_client_id_item_id`  ON `orders` (client_id, item_id);
CREATE INDEX `idx_clients_name`  ON `clients` (name);
