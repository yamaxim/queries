<?php

require_once __DIR__ . '/../src/autoload.php';

use \MaximLoboda\Utils\SqlGenerator;

$generator = new SqlGenerator();
for($i = 0; $i < 10; $i++){
    $generator->generateUsers();
    $generator->generateProducts();
    $generator->generateOrders();
}