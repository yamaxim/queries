<?php

namespace MaximLoboda;

use PDO;
use PDOException;
use PDOStatement;

/**
 * Class CsvFileHandler
 * @package Text
 */
class CsvFileHandler
{
    /**
     * @var string path to file
     */
    protected $readPath = '../cli/files/sale.csv';

    /**
     * @var string path to file
     */
    protected $writePath = '../cli/files/mismatches.csv';

    /**
     * @var int batch size
     */
    protected $batchSize = 100000;

    /**
     * @var Connection to database
     */
    protected $db;

    /**
     * @var resource file pointer to read data
     */
    protected $readFp;

    /**
     * @var resource file pointer to write data
     */
    protected $writeFp;

    /**
     * Run through the file
     */
    public function process(): void
    {
        try {
            $this->checkCli();
            $this->setFiles();
            $this->setDatabaseConnection();
            $batch = [];
            while ($row = fgetcsv($this->readFp)) {
                $batch[] = $row;
                if(sizeof((array) $batch) >= $this->batchSize){
                    $this->proceedBatch($batch);
                    $batch = [];
                }
            }
            if(sizeof((array) $batch) > 0)
                $this->proceedBatch($batch);
        } catch (FileHandlerException $e){
            echo "\r\n" . $e->getMessage();
            die();
        }
    }

    /**
     * Proceed batch
     *
     * @param $batch
     * @throws FileHandlerException
     */
    protected function proceedBatch($batch): void
    {
        try {

            // Create temporary table
            $this->createTempTable();

            // Truncate table
            $this->truncateTempTable();

            // Insert pack into the table
            $this->insertBatchToTempTable($batch);

            // Insert matches to orders table
            $this->insertMatchesToOrders();

            // Get mismatches query
            $query = $this->getMismatchesQuery();

            // Write query response to the file
            $this->mismatchesQueryToFile($query);

            // Drop table
            $this->dropTemporaryTable();

        } catch (PDOException $e) {
            throw new FileHandlerException($e->getMessage());
        }
    }

    /**
     * Set file pointers
     *
     * @throws FileHandlerException
     */
    protected function setFiles(): void
    {
        if (!file_exists($this->readPath))
            throw new FileHandlerException('File to read is not found');
        $readFp = fopen($this->readPath, 'r');
        if (!$readFp)
            throw new FileHandlerException('File to read open failed');
        $this->readFp = $readFp;
        $writeFp = fopen($this->writePath, 'w');
        if (!$writeFp)
            throw new FileHandlerException('File to write open failed');
        $this->writeFp = $writeFp;
    }

    /**
     * @throws FileHandlerException
     */
    protected function checkCli(): void
    {
        if (php_sapi_name() !== "cli")
            throw new FileHandlerException("Use cli instead");
    }

    /**
     * Set database connection
     *
     * @throws FileHandlerException
     */
    protected function setDatabaseConnection(): void
    {
        $this->db = new Connection();
    }

    /**
     * Creates temporary table.
     * Not really temporary cause of MySQL bug:
     * https://bugs.mysql.com/bug.php?id=10327 
     */
    protected function createTempTable(): void
    {
        $maketemp = "
                CREATE TABLE IF NOT EXISTS temporary_table (
                  `id` int NOT NULL AUTO_INCREMENT,
                  `product_id` int,
                  `client_id` int,
                  `comment` varchar(64),
                  PRIMARY KEY(id)
                );
            ";
        $this->db->query($maketemp);
    }

    /**
     * Truncate table
     */
    protected function truncateTempTable(): void
    {
        $truncate = "TRUNCATE TABLE temporary_table;";
        $this->db->query($truncate);
    }

    /**
     * Insert batch to temp table
     * 
     * @param $batch
     */
    protected function insertBatchToTempTable($batch): void
    {
        $values = "";
        foreach ($batch as $value) {
            if (strlen($values) > 0) $values .= ",";
            $product_id = (int) $value[0];
            $client_id = (int) $value[1];
            $comment = substr($value[2], 0, 64);
            $values .= "({$product_id},{$client_id},'{$comment}')";
        }
        $insertTemp = "
                INSERT INTO `yandex`.`temporary_table` (`product_id`, `client_id`, `comment`)
                VALUES {$values};
            ";
        $this->db->query($insertTemp);
    }

    /**
     * Insert matches to orders
     */
    protected function insertMatchesToOrders(): void
    {
        $insertMatches = "
                INSERT INTO orders(`client_id`, `item_id`, `comment`, `order_date`, `status`)
                SELECT `product_id` as `item_id`, `client_id`, `comment`, NOW() as `order_date`, 'preparing' as `status`
                FROM temporary_table
                INNER JOIN clients ON temporary_table.client_id = clients.id
                INNER JOIN merchandise ON temporary_table.product_id = merchandise.id
              ";
        $this->db->query($insertMatches);
    }

    /**
     * Get mismatches query object
     * 
     * @return PDOStatement
     */
    protected function getMismatchesQuery(): PDOStatement
    {
        $selectMismatches = "
                SELECT product_id, client_id, comment FROM temporary_table
                WHERE id NOT IN (
                    SELECT temporary_table.id
                    FROM temporary_table
                    INNER JOIN clients ON temporary_table.client_id = clients.id
                    INNER JOIN merchandise ON temporary_table.product_id = merchandise.id
                );
            ";
        $query = $this->db->query($selectMismatches);
        return $query;
    }

    /**
     * Write mismatches to csv file
     * 
     * @param $query
     */
    protected function mismatchesQueryToFile($query): void
    {
        while ($info = $query->fetch(PDO::FETCH_ASSOC)) {
            fputcsv($this->writeFp, $info);
        }
    }

    /**
     * Drop temporary table
     */
    protected function dropTemporaryTable(): void
    {
        $truncate = "DROP TABLE temporary_table;";
        $this->db->query($truncate);
    }
}