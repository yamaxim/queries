<?php

namespace MaximLoboda;

use PDO;
use PDOException;

class Connection extends PDO
{
    /**
     * @var string DB host
     */
    private $host = '172.18.0.4';

    /**
     * @var string Database name
     */
    private $db = 'yandex';

    /**
     * @var string User name
     */
    private $user = 'root';

    /**
     * @var string User password
     */
    private $password = 'secret';

    /**
     * Connection constructor.
     * @throws FileHandlerException
     */
    public function __construct()
    {
        try {
            parent::__construct(
                'mysql:host=' . $this->host . ';dbname=' . $this->db,
                $this->user,
                $this->password
            );
            $this->setAttribute(
                self::ATTR_ERRMODE,
                self::ERRMODE_EXCEPTION
            );
        } catch (PDOException $e) {
            throw new FileHandlerException("Cannot connect to database");
        }
    }
}